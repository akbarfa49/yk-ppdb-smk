package main

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"sync"

	"github.com/360EntSecGroup-Skylar/excelize/v2"
)

type dataSekolah struct {
	Kode       string
	Kompetensi string
}

//"nama", "Jenis Kelamin", "sklh", "alamat"
func main() {
	schoolID := getSchool()
	var choosen int
	fmt.Println("Pilih Sekolah :")
	for i := 0; i < len(schoolID); i++ {
		fmt.Printf("%v. %v \n", i+1, schoolID[i]["nama"])
	}
	fmt.Println("ketik nomor.")
choose:
	_, err := fmt.Scanln(&choosen)
	if choosen < 1 || choosen > len(schoolID)+1 || err != nil {
		fmt.Println("Silahkan masukkan nomor yang benar")
		goto choose
	}
	fmt.Printf("Memproses %v \n", schoolID[choosen-1]["nama"])

	if err = os.Mkdir(schoolID[choosen-1]["nama"].(string), 0764); err != nil {
		fmt.Println(err)
	}
	dataSch := getData(schoolID[choosen-1]["sekolah_id"].(string))
	var wg sync.WaitGroup
	for i := 0; i < len(dataSch); i++ {
		wg.Add(1)
		go exec(dataSch[i], schoolID[choosen-1]["nama"].(string), schoolID[choosen-1]["sekolah_id"].(string), &wg)
	}
	wg.Wait()
	fmt.Println("DONE")
}

func getSchool() []map[string]interface{} {
	req, _ := http.NewRequest("GET", "https://yogyaprov.siap-ppdb.com/sekolah/1-smk-reguler.json", nil)
	req.Header.Set("User-Agent", "Mozilla/5.0 (X11; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0")
	req.Header.Set("Accept", "application/json, text/plain, */*")
	req.Header.Set("Accept-Language", "en-US,en;q=0.5")
	req.Header.Set("Connection", "keep-alive")
	req.Header.Set("Referer", "https://yogyaprov.siap-ppdb.com/")
	req.Header.Set("Cookie", "_ga=GA1.2.1182533397.1579489792; _gid=GA1.2.593439772.1593784415; _gat_UA220414201=1; _gat_UA1004716441=1")
	req.Header.Set("Pragma", "no-cache")
	req.Header.Set("Cache-Control", "no-cache")
	req.Header.Set("Te", "Trailers")
re:
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		goto re
	}
	defer resp.Body.Close()
	reader, _ := ioutil.ReadAll(resp.Body)
	var receiver interface{}
	if err := json.Unmarshal(reader, &receiver); err != nil {
		fmt.Println(err)
	}
	var returner []map[string]interface{}
	for i := 0; i < len(receiver.([]interface{})); i++ {
		returner = append(returner, receiver.([]interface{})[i].(map[string]interface{}))
	}
	return returner
}

//[]array map [0] kode, [1] kompetensi keahlian
func getData(index string) []dataSekolah {
	var data []dataSekolah
	var wrapper dataSekolah
	req, _ := http.NewRequest("GET", "https://yogyaprov.siap-ppdb.com/sekolah/kompetensi/1-smk-reguler.json", nil)
	req.Header.Set("User-Agent", "Mozilla/5.0 (X11; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0")
	req.Header.Set("Accept", "application/json, text/plain, */*")
	req.Header.Set("Accept-Language", "en-US,en;q=0.5")
	req.Header.Set("Connection", "keep-alive")
	req.Header.Set("Referer", "https://yogyaprov.siap-ppdb.com/")
	req.Header.Set("Cookie", "_ga=GA1.2.1182533397.1579489792; _gid=GA1.2.593439772.1593784415; _gat_UA220414201=1; _gat_UA1004716441=1")
	req.Header.Set("Pragma", "no-cache")
	req.Header.Set("Cache-Control", "no-cache")
	req.Header.Set("Te", "Trailers")
re:
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		goto re
	}
	defer resp.Body.Close()
	reader, _ := ioutil.ReadAll(resp.Body)
	var receiver interface{}
	if err := json.Unmarshal(reader, &receiver); err != nil {
		fmt.Println(err)
	}
	for i := 0; i < len(receiver.(map[string]interface{})[index].([]interface{})); i++ {
		wrapper.Kode = receiver.(map[string]interface{})[index].([]interface{})[i].([]interface{})[0].(string)
		wrapper.Kompetensi = receiver.(map[string]interface{})[index].([]interface{})[i].([]interface{})[1].(string)
		data = append(data, wrapper)
	}
	return data
}

func exec(data dataSekolah, schName, schIndex string, wg *sync.WaitGroup) {
	siswa := daftarSiswa(data.Kode, schIndex)
	ch := make(chan map[string]string)

	for i := 0; i < len(siswa["data"].([]interface{})); i++ {
		go bacaData(ch, siswa["data"].([]interface{})[i].([]interface{})[2].(string))
	}
	f := excelize.NewFile()
	index := f.NewSheet("Sheet1")
	f.SetCellValue("Sheet1", "A1", "Nama")
	f.SetCellValue("Sheet1", "B1", "Jenis Kelamin")
	f.SetCellValue("Sheet1", "C1", "Asal Sekolah")
	f.SetCellValue("Sheet1", "D1", "Alamat")

	for i := 0; i < len(siswa["data"].([]interface{})); i++ {
		func(i int, value map[string]string) {
			f.SetCellValue("Sheet1", fmt.Sprintf("A%v", i), value["nama"])
			f.SetCellValue("Sheet1", fmt.Sprintf("B%v", i), value["kelamin"])
			f.SetCellValue("Sheet1", fmt.Sprintf("C%v", i), value["asal"])
			f.SetCellValue("Sheet1", fmt.Sprintf("D%v", i), value["alamat"])
		}(i+2, <-ch)
		if i == len(siswa["data"].([]interface{}))-1 {
			close(ch)
		}
	}
	f.SetActiveSheet(index)
	if err := f.SaveAs(schName + "/" + data.Kompetensi + ".xlsx"); err != nil {
		fmt.Println(err)
	}
	wg.Done()
}

func daftarSiswa(kode, schID string) map[string]interface{} {
	req, _ := http.NewRequest("GET", "https://yogyaprov.siap-ppdb.com/seleksi/reguler/smk/1-"+schID+"-"+kode+".json", nil)
	req.Header.Set("User-Agent", "Mozilla/5.0 (X11; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0")
	req.Header.Set("Accept", "application/json, text/plain, */*")
	req.Header.Set("Accept-Language", "en-US,en;q=0.5")
	req.Header.Set("Connection", "keep-alive")
	req.Header.Set("Referer", "https://yogyaprov.siap-ppdb.com/")
	req.Header.Set("Cookie", "_ga=GA1.2.1182533397.1579489792; _gid=GA1.2.593439772.1593784415; _gat_UA220414201=1; _gat_UA1004716441=1")
	req.Header.Set("Pragma", "no-cache")
	req.Header.Set("Cache-Control", "no-cache")
	req.Header.Set("Te", "Trailers")
	req.Close = true
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		fmt.Println(err)
		return nil
	}
	defer resp.Body.Close()
	reader, _ := ioutil.ReadAll(resp.Body)
	var receiver interface{}
	if err := json.Unmarshal(reader, &receiver); err != nil {
		fmt.Println(err)
		return nil
	}
	return receiver.(map[string]interface{})
}
func bacaData(ch chan<- map[string]string, no string) {
	req, _ := http.NewRequest("GET", "https://api.siap-ppdb.com/cari?no_daftar="+no, nil)
	req.Header.Set("User-Agent", "Mozilla/5.0 (X11; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0")
	req.Header.Set("Accept", "application/json, text/plain, */*")
	req.Header.Set("Accept-Language", "en-US,en;q=0.5")
	req.Header.Set("X-Requested-With", "XMLHttpRequest")
	req.Header.Set("Connection", "keep-alive")
	req.Header.Set("Referer", "https://yogyaprov.siap-ppdb.com/")
	req.Header.Set("Cookie", "_ga=GA1.2.1182533397.1579489792; _gid=GA1.2.593439772.1593784415; _gat_UA220414201=1; _gat_UA1004716441=1")
	req.Header.Set("Pragma", "no-cache")
	req.Header.Set("Cache-Control", "no-cache")
	req.Header.Set("Te", "Trailers")
redo:
	resp, err := http.DefaultClient.Do(req)
	if err != nil || resp.StatusCode != 200 {
		fmt.Println(err)
		goto redo

	}
	if err == io.EOF {
		goto redo
	}
	defer resp.Body.Close()
	reader, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println(err)
		fmt.Println("maybe EO2?")
	}
	var receiver interface{}
	sender := map[string]string{
		"nama":    "",
		"kelamin": "",
		"alamat":  "",
		"asal":    "",
	}
	if err := json.Unmarshal(reader, &receiver); err != nil {
		fmt.Println("maybe EOF3?")
		fmt.Println(err)
	}

	sender["nama"] = receiver.([]interface{})[0].([]interface{})[3].([]interface{})[2].([]interface{})[3].(string)
	sender["kelamin"] = receiver.([]interface{})[0].([]interface{})[3].([]interface{})[3].([]interface{})[3].(string)
	sender["alamat"] = receiver.([]interface{})[0].([]interface{})[3].([]interface{})[5].([]interface{})[3].(string)
	sender["asal"] = receiver.([]interface{})[0].([]interface{})[3].([]interface{})[6].([]interface{})[3].(string)
	ch <- sender
}
